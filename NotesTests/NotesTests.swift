//
//  NotesTests.swift
//  NotesTests
//
//  Created by Semen Meluzov on 20.04.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import XCTest
@testable import Notes

class NotesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testNote() {
        
        let uuid = "122121dfdfgsb"
        let title = "title"
        let content = "content"
        let color = UIColor.init(red: 0.2, green: 0.3, blue: 0.4, alpha: 0.3)
        let deleteDate = Date(timeIntervalSinceNow: 10)
        
        let note = Note(uuid: uuid, title: title, content: content, noteColor: color, deleteDate: deleteDate)
        
        guard uuid == note.uuid,
            title == note.title,
            content == note.content,
            color.isEqual(note.noteColor),
            deleteDate == note.deleteDate
        else {

            XCTFail()
            return
        }
    }
    
    func testNoteJSONParse() {
        
        var notesArray = [Note]()
        
        for i in 0...10 {
            let note = Note(title: "Note\(i)",
                            content: "Content = \(i)",
                            noteColor: UIColor.init(red: CGFloat(i) / 255,
                                                    green: CGFloat(i) / 255,
                                                    blue: CGFloat(i) / 255,
                                                    alpha: 1.0),
                            deleteDate: Date(timeIntervalSinceNow: TimeInterval(i)))
            notesArray.append(note)
        }
        
        let originalJSONArray = getJSONArrayFromNotes(notesArray: notesArray) as NSArray
        notesArray.removeAll()
        
        for jsonElement in originalJSONArray {
            
            guard let note = Note.parse(json: jsonElement as! [String : Any]) else {
                XCTFail()
                return
            }
            notesArray.append(note)
        }

        let testedJSONArray = getJSONArrayFromNotes(notesArray: notesArray) as NSArray
        
        XCTAssertEqual(originalJSONArray, testedJSONArray, "Not Equel arrays")
        
    }
    
    func getJSONArrayFromNotes(notesArray: [Note]) -> [[String : Any]]{
        
        var jsonArray = [[String : Any]]()
        for note in notesArray {
            
            jsonArray.append(note.json)
        }
        return jsonArray
    }
    
    func testDateToString() {
        
        let firstDate = Date(timeIntervalSinceNow: 10)
        let dateString = firstDate.toString()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Date.dateFormatString
        
        guard let secondDate = dateFormatter.date(from: dateString) else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(firstDate.toString(), secondDate.toString(), "Dates are not equal\nDate1 = \(firstDate)\nDate2 = \(secondDate)")
    }
    
    func testUIColorGetRGB() {
        
        let color = UIColor.init(red: 0.2, green: 0.3, blue: 0.4, alpha: 0.3)
        guard let json = color.getRGBForJSON() else {return}
        
        guard let red = json["red"],
                let green = json["green"],
                let blue = json["blue"],
                let alpha = json["alpha"]
        else {
            return
        }
        let secondColor = UIColor.init(red: red, green: green, blue: blue, alpha: alpha)
        XCTAssertEqual(color, secondColor, "Colors are not equal")
    }
    
    func testSaveAndReadFile() {
     
     
        let noteBook = DummyNoteBook()
     
        for i in 0...10 {
            noteBook.addNew(note: Note(title: "Note\(i)",
                                       content: "Content = \(i)",
                                       noteColor: UIColor.init(red: CGFloat(i) / 255,
                                                               green: CGFloat(i) / 255,
                                                               blue: CGFloat(i) / 255,
                                                               alpha: 1.0),
                                       deleteDate: Date(timeIntervalSinceNow: TimeInterval(i))))
        }
        
        let originalCollection = getNoteComponent(dict: noteBook.notesCollection) as NSDictionary
        noteBook.saveAll()
        noteBook.deleteAll()
        noteBook.loadFile()
        
        //Удалить все заметки после загрузки -> тест падает
        //noteBook.deleteAll()
        
        let loadCollection = getNoteComponent(dict: noteBook.notesCollection) as NSDictionary

        XCTAssertEqual(originalCollection, loadCollection, "BadWriteAndReadTest:\nOriginal =  \(originalCollection)\nLoad = \(loadCollection)")
    }
    
    func getNoteComponent(dict : [String : Note]) -> [String : Any] {
        
        var notes = [String : Any]()
        for (key, note) in dict {
            
            notes[key] = note.json
        }
        return notes
    }
    
    
    func testDeleteDate() {
        
        let maxNotes = 10
        let expect = expectation(description: "11")
        
        let noteBook = DummyNoteBook()
        for i in 0...maxNotes {
            noteBook.addNew(note: Note(title: "Note\(i)",
                            content: "Content = \(i)",
                deleteDate: i % 2 == 0 ? Date(timeIntervalSinceNow: TimeInterval(i / 2+1)) : nil))
        }
        
        //Запоминаю все uuid тех заметок, у которых есть deleteDate
        var uuidNoteWithDate = [String]()
        for (_, note) in noteBook.notesCollection {
            if note.deleteDate != nil {
                uuidNoteWithDate.append(note.uuid)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(maxNotes)) {
            
            //Проверяю все ли заметки с deleteDate удалились
            for uuid in uuidNoteWithDate {
                if noteBook.notesCollection[uuid] != nil {
                    XCTFail()
                }
            }
            expect.fulfill()
        }
        
        waitForExpectations(timeout: Double(maxNotes*2), handler: nil)
    }

}
