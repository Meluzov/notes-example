//
//  AuthViewController.swift
//  Notes
//
//  Created by Semen Meluzov on 02.06.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController, UIWebViewDelegate {
    
    var didAuth: ((_ authToken : String?) -> ())? = nil
    
    @IBOutlet weak var webView: UIWebView!
    
    let clientID = "0d0970774e284fa8ba9ff70b6b06479a"
    let clientSecret = "a7ee508b75664955bd5d5ad59f66b196"
    var token : String?
    
    override func viewDidLoad() {
     
        webView.delegate = self
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "oauth.yandex.ru"
        urlComponents.path = "/authorize"
        urlComponents.queryItems = [
            URLQueryItem(name: "response_type", value: "code"),
            URLQueryItem(name: "client_id", value: "0d0970774e284fa8ba9ff70b6b06479a")
        ]
        
        if let url = urlComponents.url {
            
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    func remove() {
        didAuth?(token)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        
        guard let url = request.url else {return false }
        let path = URLComponents(url: url, resolvingAgainstBaseURL: false)?.path
        let queryItems = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems

        let code = queryItems?.filter({$0.name == "code"}).first
        
        if code != nil && path == "/verification_code"{
            
            getOAuthTokenBy(code: code)
        }
        
        return true
    }
    
    func getOAuthTokenBy(code : URLQueryItem?) {
        
        guard let codeString = code?.value else {
            remove()
            return
        }
        
        guard let url = URL(string: "https://oauth.yandex.ru/token") else {
            print("URL not found")
            remove()
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let bodyData = "grant_type=authorization_code&code=\(codeString)&client_id=\(clientID)&client_secret=\(clientSecret)"
        request.httpBody = bodyData.data(using: String.Encoding.utf8);
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            guard let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any] else {
                self.remove()
                return
            }
            self.token = json["access_token"] as? String
            
            if let error = error {
                print(error.localizedDescription)
                self.remove()
                return
            }
            if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200..<300:
                    print("Status AUTH : \(response.statusCode)")
                    break
                default:
                    print("Status : \(response.statusCode)")
                    self.remove()
                    return
                }
            }
            self.remove()
        }
        
        task.resume()
    }

    
}
