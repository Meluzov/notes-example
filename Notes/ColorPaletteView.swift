//
//  ColorPaletteView.swift
//  Notes
//
//  Created by Semen Meluzov on 05.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

extension Comparable {
    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}

protocol ColorPaletteViewDelegate: class {
    
    func didChange(color: UIColor)
}

class ColorPaletteView : UIView {
    
    weak var delegate: ColorPaletteViewDelegate?
    var colorCursorView : ColorCursorView!
    
    var brightness : CGFloat = 1.0 {
        didSet {
            guard let cursor = colorCursorView else { return }
            let color = getColorAtPoint(point: cursor.center)
            cursor.update(color: color)
            self.delegate?.didChange(color: color)
        }
    }
    
    private func initialize() {
        
        self.clipsToBounds = true
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(ColorPaletteView.panGesture(sender:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ColorPaletteView.tapGesture(sender:)))
        addGestureRecognizer(panGesture)
        addGestureRecognizer(tapGesture)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
        
    }
    
    override func layoutSubviews() {
        
        guard let cursor = colorCursorView else {
            
            return
        }
        let position = getPointForColor(color: cursor.selectedColor)
        let xPos = position.x * self.frame.width
        let yPos = (1-position.y) * self.frame.height
        cursor.center = CGPoint(x: xPos, y: yPos)
    }
    
    func addCuror(color: UIColor) {
        
        let width : CGFloat = 50
        colorCursorView = ColorCursorView(frame: CGRect(x: 0,
                                                        y: 0,
                                                        width: width,
                                                        height: width))
        colorCursorView.selectedColor = color
        addSubview(colorCursorView)
    }
    
    func getColorAtPoint(point:CGPoint) -> UIColor {
        
        let xPos = point.x.clamped(to: 0...self.frame.width)
        let yPos = point.y.clamped(to: 0...self.frame.height)
        let saturation : CGFloat = (self.frame.height - yPos) / self.frame.height
        let hue = xPos / self.bounds.width
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    func getPointForColor(color:UIColor) -> CGPoint {
        
        var hue : CGFloat = 0
        var saturation : CGFloat=0
        var brightness : CGFloat = 0
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: nil);
        return CGPoint(x: hue, y: saturation)
    }
    
    //MARK: - UIGestureRecognizer
    func tapGesture(sender: UITapGestureRecognizer){
        didGesture(sender: sender)
    }
    
    func panGesture(sender: UIPanGestureRecognizer){
        didGesture(sender: sender)
    }
    
    func didGesture(sender : UIGestureRecognizer) {
        
        let point = sender.location(in: self)
        var color = getColorAtPoint(point: point)

        var xPos = point.x
        var yPos = point.y
        
        if xPos < 0 || xPos > self.bounds.maxX {
            xPos = colorCursorView.center.x
        }
        if yPos < 0 || yPos > self.bounds.maxY {
            yPos = colorCursorView.center.y
        }
        
        colorCursorView.update(position: CGPoint(x: xPos, y : yPos), color: color)
        self.delegate?.didChange(color: color)
    }
    
    func deviceDidRotate() {
        let position = getPointForColor(color: colorCursorView.selectedColor)
        colorCursorView.update(position: CGPoint(x: position.x, y : position.y))
    }
}
