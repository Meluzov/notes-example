//
//  ColorPickerView.swift
//  Notes
//
//  Created by Semen Meluzov on 05.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ColorPickerView: UIView, ColorPaletteViewDelegate {

    @IBOutlet weak var colorInfoView: ColorInfoView!
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var colorPaletteView: ColorPaletteView!
    @IBOutlet weak var brightnessView: UIView!
    
    var selectedColor = UIColor.black
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    

    func commonInit() {
        guard let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last else {
            return
        }
        guard let nibView = Bundle(for: type(of: self)).loadNibNamed(nibName, owner: self, options: nil)?.last as? UIView else {
            return
        }
        
        addSubview(nibView)
        nibView.frame = bounds
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        colorPaletteView.layer.borderWidth = 2.0
        colorPaletteView.layer.borderColor = UIColor.black.cgColor
        
        colorPaletteView.delegate = self
    }
    
    func setStartColor() {

        //Set color to views
        colorPaletteView.brightness = selectedColor.hsba.b
        colorPaletteView.addCuror(color: selectedColor)
        colorInfoView.setBackground(color: selectedColor)
        brightnessSlider.value = Float(selectedColor.hsba.b)
        brightnessView.alpha = 1 - selectedColor.hsba.b
    }
    
    //MARK: - ColorPaletteViewDelegate
    func didChange(color: UIColor) {
        
        selectedColor = color
        colorInfoView.setBackground(color: color)
    }
    
    //MARK: - Actions
    @IBAction func sliderValueChangedAction(_ sender: UISlider) {
        
        colorPaletteView.brightness = CGFloat(sender.value)
        brightnessView.alpha = 1 - CGFloat(sender.value)
    }

}
