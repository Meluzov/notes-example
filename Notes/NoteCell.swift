//
//  NoteCell.swift
//  Notes
//
//  Created by Semen Meluzov on 13.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class NoteCell: UICollectionViewCell {
    
    @IBOutlet weak var cornerColorView: CornerColorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var deleteView: UIView!

}
