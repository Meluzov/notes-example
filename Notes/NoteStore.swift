//
//  NoteStore.swift
//  Notes
//
//  Created by Semen Meluzov on 05.06.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CoreData

class NoteStore {
    
    static func convert(entity: NoteEntity) -> Note? {
        
        guard let uuid = entity.uuid,
                let title = entity.title,
                let content = entity.content,
                let hexColor = entity.color else { return nil }
        
        let note = Note(uuid: uuid,
                        title: title,
                        content: content,
                        noteColor: UIColor.init(hex: hexColor),
                        deleteDate: entity.deleteDate == -1 ? nil : Date(timeIntervalSince1970: entity.deleteDate))
        return note
    }
    
    static func convert(note: Note, toEntity entity: NoteEntity ) -> NoteEntity {
        
        entity.uuid = note.uuid
        entity.title = note.title
        entity.content = note.content
        
        if note.deleteDate == nil {
            entity.deleteDate = -1
        } else {
            entity.deleteDate = Double(note.deleteDate!.timeIntervalSince1970)
        }
        entity.color = note.noteColor.hexString
        return entity
    }
    
}
