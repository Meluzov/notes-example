//
//  LoadCacheOperation.swift
//  Notes
//
//  Created by Semen Meluzov on 24.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CoreData

class LoadCacheOperation: AsyncOperation {
    
    var managedObjectContext : NSManagedObjectContext {
        return CoreDataManager.shared.managedObjectContext
    }
    
    override func main() {
        
        managedObjectContext.performAndWait {[weak self] in
            
            guard let sself = self else { return }

            let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
            
            do {
                let notesEntity = try sself.managedObjectContext.fetch(request)
                
                if notesEntity.isEmpty {
                    sself.finishFailure()
                    return
                }
                for noteEntity in notesEntity {
                    
                    if dummyNoteBookCache == nil {
                        dummyNoteBookCache = DummyNoteBook()
                    }
                    
                    guard let note = NoteStore.convert(entity: noteEntity) else {
                        sself.finishFailure()
                        return
                    }
                    dummyNoteBookCache.addNew(note: note)
                }
                
            } catch {
                sself.finishFailure()
                return
            }
            
            sself.finishSuccess()
        }
        
    }
}
