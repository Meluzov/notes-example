//
//  UIColor+getHEX.swift
//  Notes
//
//  Created by Semen Meluzov on 06.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

extension UIColor
{

    var hexString: String {
        guard let components = self.cgColor.components else {
            return "Bad UIColor"
        }
        
        let red = Float(components[0])
        let green = Float(components[1])
        let blue = Float(components[2])
        return String(format: "#%02lX%02lX%02lX", lroundf(red * 255), lroundf(green * 255), lroundf(blue * 255))
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        
        if (hex.hasPrefix("#")) {
                scanner.scanLocation = 1
        }else {
            scanner.scanLocation = 0

        }

        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }

}
