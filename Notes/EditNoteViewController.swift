//
//  EditNoteViewController.swift
//  Notes
//
//  Created by Semen Meluzov on 04.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

enum EditMode {
    case New
    case Edit
}

protocol EditNoteViewControllerDelegate: class {
    
    func didCreateNew(note: Note)
    func didEdit(note: Note)
}

class EditNoteViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, ColorViewDelegate, ColorPickerViewControllerDelegate {
    
    weak var delegate : EditNoteViewControllerDelegate?
    
    //MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!

    @IBOutlet weak var destroyDatePicker: UIDatePicker!
    
    @IBOutlet weak var dateSwitch: UISwitch!
    @IBOutlet var colorViewCollection: [ColorView]!
    @IBOutlet weak var paletteColorView: ColorView!
    @IBOutlet weak var defaultColorView: ColorView!
    
    @IBOutlet weak var datePickerHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Properties
    //default value of color
    var selectedPaletteColor = UIColor.init(hue: 0.5, saturation: 0.5, brightness: 0.5, alpha: 1.0)
    var noteColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    var colorPickerPresentationManager = ColorPickerPresentationManager()
    
    var pushNote : Note!
    var editMode : EditMode!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(EditNoteViewController.saveButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = saveButton

        titleTextField.delegate = self
        colorViewCollection.forEach({$0.delegate = self})
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self , action: #selector(tapGesture(sender:)))
        contentView.addGestureRecognizer(tapGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditNoteViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditNoteViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        defaultColorView.isSelected = true
        paletteColorView.addPaletteView()
        
        setUI()
    }
    
    func setUI() {
        
        titleTextField.text = pushNote.title
        contentTextView.text = pushNote.content
        
        if pushNote.noteColor != UIColor.white {
            noteColor = pushNote.noteColor
            
            selectedPaletteColor = UIColor.init(hue: noteColor.hsba.h, saturation: noteColor.hsba.s, brightness: noteColor.hsba.b, alpha: noteColor.hsba.a)
            paletteColorView.removePaletteView()
            paletteColorView.backColor = selectedPaletteColor
            paletteColorView.isSelected = true
            
            for paletteColor in colorViewCollection {
                if !paletteColor.isEqual(paletteColorView) {
                    paletteColor.isSelected = false
                }
            }
        }
        
    }

    
    //MARK: - Notifications
    func keyboardWillShow(notification:NSNotification) {
       
        var userInfo = notification.userInfo!
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        scrollView.contentInset = contentInsets
    }
    
    func keyboardWillHide(notification:NSNotification) {
        
        scrollView.contentInset = UIEdgeInsets.zero
    }

    //MARK: - UIGetureRecognizer
    func tapGesture(sender: UIPanGestureRecognizer) {
        
        if contentTextView.isFirstResponder {
            contentTextView.resignFirstResponder()
        }
        
        if titleTextField.isFirstResponder {
            titleTextField.resignFirstResponder()
        }
    }
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.isEqual(titleTextField) {
            textField.resignFirstResponder()
        }
        contentTextView.becomeFirstResponder()
        return false
    }
    
    //MARK: - ColorViewDelegate
    func didTapTo(sender: ColorView) {
        
        sender.isSelected = true
        noteColor = sender.color
        for paletteColor in colorViewCollection {
            if !paletteColor.isEqual(sender) {
                paletteColor.isSelected = false
            }
        }
    }
    
    func didLongPress(sender: ColorView) {
        
        if sender.isEqual(paletteColorView) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let colorPickerVC = storyboard.instantiateViewController(withIdentifier: "ColorPickerViewController") as? ColorPickerViewController else {
                return
            }
            colorPickerVC.initColor = selectedPaletteColor
            colorPickerVC.delegate = self
            colorPickerVC.transitioningDelegate = colorPickerPresentationManager
            colorPickerVC.modalPresentationStyle = .custom
            self.present(colorPickerVC, animated: true, completion: nil)
        }
    }
    
    //MARK: - ColorPickerViewControllerDelegate
    func colorPickerVCDismissed (noteColor: UIColor) {
        
        selectedPaletteColor = noteColor
        self.noteColor = noteColor
        
        paletteColorView.removePaletteView()
        paletteColorView.backColor = noteColor
        paletteColorView.isSelected = true
        for paletteColor in colorViewCollection {
            if !paletteColor.isEqual(paletteColorView) {
                paletteColor.isSelected = false
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func destroyDateSwitchAction(_ sender: UISwitch) {
        
        datePickerHeightConstraint.constant = sender.isOn ? 216 : 0
        destroyDatePicker.isHidden = !sender.isOn
    }
    
    func saveButtonAction(sender: UIBarButtonItem) {
        
        let destroyDate : Date? = dateSwitch.isOn ? destroyDatePicker.date : nil
        let note = Note(uuid: pushNote.uuid, title: titleTextField.text!, content: contentTextView.text, noteColor: noteColor, deleteDate: destroyDate)
        
        if editMode == .New {
            self.delegate?.didCreateNew(note: note)
        } else if editMode == .Edit {
            self.delegate?.didEdit(note: note)
        }
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
}



