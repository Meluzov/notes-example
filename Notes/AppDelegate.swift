//
//  AppDelegate.swift
//  Notes
//
//  Created by Semen Meluzov on 20.04.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CocoaLumberjack
import CoreData

var dummyNoteBookCache : DummyNoteBook!
var dummyNoteBookNetwork : DummyNoteBook!

var oAuthToken : String?
let oAuthTokenKey = "OAuthToken"

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //DDLog.add(DDTTYLogger.sharedInstance)
        
        if let token = UserDefaults.standard.string(forKey: oAuthTokenKey) {
            oAuthToken = token
        }
        
        CoreDataManager.shared.migration()
        
        let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
        do {
            
            let notes = try CoreDataManager.shared.managedObjectContext.fetch(request) 
            
            for note in notes {
                print("UUid = \(note.uuid), Title = \(note.title), Content = \(note.content), Date = \(note.deleteDate)")
            }
            
        } catch {
            
        }

        //start cache
        
//        dummyNoteBookCache = DummyNoteBook()
//        for i in 0...5 {
//
//            dummyNoteBookCache.addNew(note: Note(title: "\(i)",
//                content: "Content of NoteNumber \(i)",
//                noteColor: UIColor.init(hue: CGFloat(i) / 10,
//                                        saturation: CGFloat(i) / 10,
//                                        brightness: CGFloat(i) / 10,
//                                        alpha: 1.0),
//                deleteDate: nil))
//        }
        
        
        //start network
        
//        dummyNoteBookNetwork = DummyNoteBook()
//        for i in 0...5000 {
//            
//            dummyNoteBookNetwork.addNew(note: Note(title: "\(i)",
//                content: "Content of NoteNumber \(i)",
//                noteColor: UIColor.init(hue: CGFloat(i) / 10,
//                                        saturation: CGFloat(i) / 10,
//                                        brightness: CGFloat(i) / 10,
//                                        alpha: 1.0),
//                deleteDate: nil))
//        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        CoreDataManager.shared.saveContext()
    }
}

