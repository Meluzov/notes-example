//
//  PostToServerOperation.swift
//  Notes
//
//  Created by Semen Meluzov on 24.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class PostToServerOperation: AsyncOperation {

    private var note: Note!
    private var mode: Mode
    private var request : URLRequest!
    
    var failureRequest : (()->())? = nil
    
    init(withNote note: Note, mode: Mode) {
        self.note = note
        self.mode = mode
    }

    override func main() {
        
        switch mode {
        case .Save:
            let url = URL(string: "http://notes.mrdekk.ru/notes")
            guard let urlPost = url else {
                print("URL = \(url) not found")
                finishFailure()
                return
            }
            request = URLRequest(url: urlPost)
            request.httpMethod = "POST"
            request.httpBody = try! JSONSerialization.data(withJSONObject: note.json)
            
        case .Delete:
            let url = URL(string: "http://notes.mrdekk.ru/notes/\(note.uuid)")
            guard let urlDelete = url else {
                print("URL = \(url) not found")
                finishFailure()
                return
            }
            request = URLRequest(url: urlDelete)
            request.httpMethod = "DELETE"
            
        case .Edit:
            let url = URL(string: "http://notes.mrdekk.ru/notes/\(note.uuid)")
            guard let urlDelete = url else {
                print("URL = \(url) not found")
                finishFailure()
                return
            }
            request = URLRequest(url: urlDelete)
            request.httpMethod = "PUT"
            request.httpBody = try! JSONSerialization.data(withJSONObject: note.json)
        }
        
        guard let token = oAuthToken else {
            finishFailure()
            return
        }
        let headers = [
            "Authorization": "OAuth \(token)",
            "Content-Type": "application/json; charset=utf-8"
        ]
        request.allHTTPHeaderFields = headers
        task()
    }
    
    func task() {
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            guard let json = try? JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any] else {
                
                return
            }
            print("JSON Answer = \(json)")
            
            
            if let error = error {
                self.finishFailure()
                print(error.localizedDescription)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    print("Status : \(response.statusCode)")
                    break
                case 500:
                    print("Status : \(response.statusCode)")
                    self.failureRequest?()
                    self.finishFailure()
                    return
                default:
                    self.finishFailure()
                    print("Status : \(response.statusCode)")
                    return
                }
            }
            self.finishSuccess()
        }
        
        task.resume()
    }
}
