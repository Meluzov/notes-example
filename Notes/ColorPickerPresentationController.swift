//
//  ColorPickerPresentationController.swift
//  Notes
//
//  Created by Semen Meluzov on 12.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class ColorPickerPresentationController: UIPresentationController {
    
    var dimmingView: UIView!
    
    override var frameOfPresentedViewInContainerView: CGRect {
        
        
        var frame: CGRect = .zero
        
        guard let container = containerView else {return frame}
        frame.size = size(forChildContentContainer: presentedViewController,
                          withParentContainerSize: container.bounds.size)
        
        frame.origin = CGPoint(x: container.frame.midX - frame.size.width / 2,
                               y: container.frame.midY - frame.size.height / 2)
        
        return frame
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        setupDimmingView()
    }
    
    override func presentationTransitionWillBegin() {
        
        
        containerView?.insertSubview(dimmingView, at: 0)
        
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[dimmingView]|",
                                           options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        NSLayoutConstraint.activate(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[dimmingView]|",
                                           options: [], metrics: nil, views: ["dimmingView": dimmingView]))
        
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
    }
    
    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0.0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    
    override func size(forChildContentContainer container: UIContentContainer,
                       withParentContainerSize parentSize: CGSize) -> CGSize {
        
        let width = parentSize.width * (4/5)
        let height = parentSize.height * (1/2)
        return CGSize(width: width, height: height)
    }

}

private extension ColorPickerPresentationController {
    func setupDimmingView() {
        
        dimmingView = UIView()
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        dimmingView.alpha = 0.0
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        dimmingView.addGestureRecognizer(recognizer)
    }
    
    dynamic func handleTap(recognizer: UITapGestureRecognizer) {
        presentingViewController.dismiss(animated: true)
    }
}
