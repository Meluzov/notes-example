//
//  ColorPickerViewController.swift
//  Notes
//
//  Created by Semen Meluzov on 06.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

protocol ColorPickerViewControllerDelegate: class {
    
    func colorPickerVCDismissed (noteColor: UIColor)
}

class ColorPickerViewController: UIViewController {

    weak var delegate : ColorPickerViewControllerDelegate?
    @IBOutlet weak var colorPickerView: ColorPickerView!
    var initColor : UIColor?
    
    override func viewDidLoad() {

        if let color = initColor {
            colorPickerView.selectedColor = color
        } else {
            colorPickerView.selectedColor = UIColor.green
        }
        colorPickerView.setStartColor()
        
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        
        self.delegate?.colorPickerVCDismissed(noteColor: colorPickerView.selectedColor)
        dismiss(animated: true, completion: nil)
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        
//        super.viewWillTransition(to: size, with: coordinator)
//        coordinator.animate(alongsideTransition: nil, completion: { _ in
//            
//            self.colorPickerView.colorPaletteView.deviceDidRotate()
//
//        })
//    }
    
    
}
