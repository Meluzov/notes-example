//
//  NoteVC+Operations.swift
//  Notes
//
//  Created by Semen Meluzov on 05.06.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

extension NotebookViewController {
    
    func loadNotes() {
        
        let loadCacheOperation = LoadCacheOperation()
        let updateOperation = UpdateCollectionViewOperation(collectionView: collectionView)
        
        loadCacheOperation.failure = {[weak self] in
            guard let sself = self else {return}
            sself.createRequestOperation(DependingOn: updateOperation)
        }
        
        updateOperation.addDependency(loadCacheOperation)
        queueManager.add(operation: loadCacheOperation)
        queueManager.add(operation: updateOperation)
    }
    
    func createRequestOperation(DependingOn updateOperation: UpdateCollectionViewOperation) {
        
        let requestNetworkOperation = RequestNetworkOperation()
        
        requestNetworkOperation.failureRequest = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
                guard let sself = self else {return}
                sself.createRequestOperation(DependingOn: updateOperation)
            })
        }
        requestNetworkOperation.success = {
            let saveCacheOperation = SaveCacheOperation(withNotes: requestNetworkOperation.notesArray, mode: .Save)
            updateOperation.addDependency(saveCacheOperation)
            self.queueManager.add(operation: saveCacheOperation)
        }
        
        updateOperation.addDependency(requestNetworkOperation)
        self.queueManager.add(operation: requestNetworkOperation)
    }
    
    func save(note: Note, mode: Mode) {
        
        let saveCacheOperation = SaveCacheOperation(withNotes: [note], mode: mode)
        createPostOperation(note: note, mode: mode)
        let updateOperation = UpdateCollectionViewOperation(collectionView: collectionView)
        
        updateOperation.addDependency(saveCacheOperation)
        queueManager.add(operation: saveCacheOperation)
        queueManager.add(operation: updateOperation)
        
    }
    
    func createPostOperation(note: Note, mode: Mode) {
        let postToServerOperation = PostToServerOperation(withNote: note, mode: mode)
        postToServerOperation.failureRequest = {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
                guard let sself = self else {return}
                sself.createPostOperation(note: note, mode: mode)
            })
        }
        queueManager.add(operation: postToServerOperation)
    }

}
