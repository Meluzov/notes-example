//
//  Note.swift
//  Notes
//
//  Created by Semen Meluzov on 22.04.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CocoaLumberjack

class DeallocPrinter {
    deinit {
        
        //DDLogInfo("Note deallocated")
    }
}

public struct Note {
    //Для проверок
    let printer = DeallocPrinter()
    
    public let uuid : String
    public let title : String
    public let content : String
    public let noteColor : UIColor
    public let deleteDate : Date?
    
    public init(uuid : String = UUID().uuidString, title : String, content : String, noteColor : UIColor = UIColor.white, deleteDate : Date?) {
        
        self.uuid = uuid.lowercased()
        self.title = title
        self.content = content
        
        self.noteColor = noteColor
        
        if let date = deleteDate {
            self.deleteDate = date
        } else {
            self.deleteDate = nil
        }
    }
}
