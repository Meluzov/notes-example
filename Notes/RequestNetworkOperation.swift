//
//  RequestNetworkOperation.swift
//  Notes
//
//  Created by Semen Meluzov on 24.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit


class RequestNetworkOperation: AsyncOperation {
    
    let url = URL(string: "http://notes.mrdekk.ru/notes")
    private var request : URLRequest!
    var failureRequest : (()->())? = nil

    var notesArray : [Note]!
    override func main() {
        
        guard let url = url else {
            finishFailure()
            return
        }
        
        request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        guard let token = oAuthToken else {
            return
        }
        
        let headers = [
            "Authorization": "OAuth \(token)",
            "Content-Type": "application/json; charset=utf-8"
        ]
        
        request.allHTTPHeaderFields = headers
        
        task()
    }

    func task() {
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            if let error = error {
                self.finishFailure()
                print(error.localizedDescription)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<300:
                    print("Status : \(response.statusCode)")
                    //self.stopTimer()
                    break
                case 500:
                    print("Status : \(response.statusCode)")
                    self.failureRequest?()
                    self.finishFailure()
                    //self.startTimer()
                    return
                default:
                    self.finishFailure()
                    print("Status : \(response.statusCode)")
                    return
                }
            }
            
            guard let jsonArray = try? JSONSerialization.jsonObject(with: data!, options: []) as! [[String : Any]] else {
                self.finishFailure()
                return
            }
            print("JSONARRAY GET = \(jsonArray)")
            self.notesArray = [Note]()
            for noteData in jsonArray {
                
                guard let note = Note.parse(json: noteData) else {
                    return
                }
                self.notesArray.append(note)
            }
            self.finishSuccess()
        }
        
        task.resume()

    }
}
