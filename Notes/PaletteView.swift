//
//  PaletteView.swift
//  Notes
//
//  Created by Semen Meluzov on 06.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

@IBDesignable class PaletteView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        for y in stride(from: 0, to: rect.height, by: 1) {
            
            let saturation : CGFloat = (rect.height - y) / rect.height
            
            for x in stride(from: 0, to: rect.width, by: 1) {
                let hue = x / rect.width
                let color = UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0)
                context.setFillColor(color.cgColor)
                context.fill(CGRect(x:x, y:y, width:1,height:1))
            }
        }
    }
}
