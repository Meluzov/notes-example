//
//  NotebookViewController.swift
//  Notes
//
//  Created by Semen Meluzov on 13.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import WebKit

class NotebookViewController: UICollectionViewController, EditNoteViewControllerDelegate, WKUIDelegate {

    let queueManager = QueueManager()
    
    var sliderView : UISlider!
    var sliderLastValue : Float!
    let maxItemsPerRow : CGFloat = 3
    
    var maxItemWidth : CGFloat {
        return self.view.frame.width - 2 * sectionInsets.left - 1
    }
    
    var minItemWidth : CGFloat {
        get {
            let paddingSpace = sectionInsets.left * (maxItemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            return availableWidth / maxItemsPerRow - 1
        }
    }
    
    var midItemWidth : CGFloat {
        return (maxItemWidth + minItemWidth) / 2
    }
    
    var widthPerItem : CGFloat!


    let reuseIdentifier = "NoteCell"
    let sectionInsets = UIEdgeInsets(top: 20.0, left: 10.0, bottom: 20.0, right: 10.0)
    
    var isEditMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBarItems()
        widthPerItem = midItemWidth
        if oAuthToken != nil {
            loadNotes()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if oAuthToken == nil {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let authViewVcontroller = storyboard.instantiateViewController(withIdentifier: "AuthViewController") as? AuthViewController else {
                return
            }
            present(authViewVcontroller, animated: true, completion: nil)
            authViewVcontroller.didAuth = {(token) in
                print("TOKEN = \(token)")
                guard let authToken = token else {
                    return
                }
                oAuthToken = authToken
                UserDefaults.standard.set(authToken, forKey: oAuthTokenKey)
                self.loadNotes()
            }
        }
    }
    

    //MARK: Actions
    @IBAction func addNoteAction(_ sender: UIBarButtonItem) {
        
        pushToEditVC(note: nil, mode: .New)
    }
    
    func pushToEditVC(note: Note?, mode: EditMode) {
        
        let pushNote = note != nil ? note : Note(title: "", content: "", deleteDate: nil)
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let editNoteVC = storyboard.instantiateViewController(withIdentifier: "EditNoteViewController") as? EditNoteViewController else {
            return
        }
    
        editNoteVC.pushNote = pushNote!
        editNoteVC.delegate = self
        editNoteVC.editMode = mode
        self.navigationController?.toolbar.isHidden = true
        self.navigationController?.pushViewController(editNoteVC, animated: true)
    }

    //MARK: EditNoteViewControllerDelegate
    func didCreateNew(note: Note) {

        save(note: note, mode: .Save)
    }
    
    func didEdit(note: Note) {
        
        save(note: note, mode: .Edit)
    }
        
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: nil, completion: { _ in
            
            self.widthPerItem = self.minItemWidth + CGFloat(self.sliderView.value) * (self.maxItemWidth - self.minItemWidth)
            self.collectionViewLayout.invalidateLayout()
        })
    }
}

extension NotebookViewController {
    
    func addBarItems() {
        
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        sliderView = UISlider()
        sliderView.addTarget(self, action: #selector(NotebookViewController.sliderAction(sender:)), for: .valueChanged)
        
        sliderView.value = 0.5
        sliderLastValue = sliderView.value
        let customItem = UIBarButtonItem.init(customView: sliderView)
        self.toolbarItems = [spaceItem, customItem, spaceItem]
        
        let editButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(NotebookViewController.editButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = editButton
    }

    func sliderAction(sender: UISlider) {
        
        widthPerItem =  widthPerItem + CGFloat(sender.value - sliderLastValue) * (maxItemWidth - minItemWidth)
        self.sliderLastValue = sender.value
        
        collectionView?.performBatchUpdates({
            self.collectionViewLayout.invalidateLayout()
            self.collectionView?.setCollectionViewLayout(self.collectionViewLayout, animated: true)
        }, completion: nil)
    }
    
    func editButtonAction(sender: UIBarButtonItem) {
        
        isEditMode = !isEditMode
        
        var item = UIBarButtonSystemItem.edit
        if isEditMode {
            item = UIBarButtonSystemItem.done
        }
        let editButton = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(NotebookViewController.editButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = editButton
        collectionView?.reloadData()
        
    }
}



