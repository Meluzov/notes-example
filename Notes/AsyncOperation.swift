//
//  Operations.swift
//  Notes
//
//  Created by Semen Meluzov on 21.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit


class AsyncOperation : Operation {
    
    var success: (() -> ())? = nil
    var failure: (() -> ())? = nil

    fileprivate var _executing = false
    fileprivate var _finished = false

    override func start() {
        guard !isCancelled else {
            finishSuccess()
            return
        }
        
        willChangeValue(forKey: "isExecuting")
        _executing = true
        main()
        didChangeValue(forKey: "isExecuting")
    }
    
    override func main() {
        // NOTE: should be overriden
        finishSuccess()
    }
    
    func finishSuccess() {
        success?()
        
        willChangeValue(forKey: "isFinished")
        _finished = true
        didChangeValue(forKey: "isFinished")
    }
    
    func finishFailure() {
        failure?()
        
        willChangeValue(forKey: "isFinished")
        _finished = true
        didChangeValue(forKey: "isFinished")
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    override var isFinished: Bool {
        return _finished
    }
}

