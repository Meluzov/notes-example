//
//  ColorPickerPresentationManager.swift
//  Notes
//
//  Created by Semen Meluzov on 12.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class ColorPickerPresentationManager: NSObject {
    
}

extension ColorPickerPresentationManager: UIViewControllerTransitioningDelegate {
    
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        let presentationController = ColorPickerPresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.delegate = self

        return presentationController
    }
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        return ColorPickerAnimator(isPresentation: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ColorPickerAnimator(isPresentation: false)
    }
}

// MARK: - UIAdaptivePresentationControllerDelegate
extension ColorPickerPresentationManager: UIAdaptivePresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        if traitCollection.verticalSizeClass == .compact {
            return .overFullScreen
        } else {
            return .none
        }
    }
}
