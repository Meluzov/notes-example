//
//  SelectionIndicatorView.swift
//  Notes
//
//  Created by Semen Meluzov on 05.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class SelectionIndicatorView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let strokeLineWidth : CGFloat = 2.0
        
        context.setLineWidth(strokeLineWidth)
        context.setStrokeColor(UIColor.black.cgColor)
        
        let radius = self.frame.width / 2 - strokeLineWidth
        let center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        context.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
        context.strokePath()
        
        
        let linePoint1 = CGPoint(x: 2 * strokeLineWidth, y: self.frame.height / 2)
        let linePoint2 = CGPoint(x: self.frame.width / 2, y: self.frame.height - 2 * strokeLineWidth)
        let linePoint3 = CGPoint(x: self.frame.width * 3 / 4, y: 2 * strokeLineWidth)
        context.move(to: linePoint1)
        context.addLine(to: linePoint2)
        context.addLine(to: linePoint3)
        context.strokePath()

    }
    
}
