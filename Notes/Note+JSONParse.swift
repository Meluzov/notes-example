//
//  Note+JSONParse.swift
//  Notes
//
//  Created by Semen Meluzov on 22.04.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CocoaLumberjack

extension Note {
    
    public var json: [String: Any] {
        
        var jsonObject : [String: Any] = [
            "uid" : uuid,
            "title" : title,
            "content" : content
        ]
        
        if !noteColor.isEqual(UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)) {
            jsonObject["color"] = noteColor.hexString
        }
        
        if let date = deleteDate {
            jsonObject["destroy_date"] = Int(date.timeIntervalSince1970)
        }
        
        return jsonObject
    }
    
    public static func parse(json: [String: Any]) -> Note? {
        
        guard let uuid = json["uid"] as? String,
            let title = json["title"] as? String,
            let content = json["content"] as? String
            else {
                
                DDLogInfo("JSON bad string")
                return nil
        }
        
        var noteColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if let hexString = json["color"] as? String {
            noteColor = UIColor.init(hex: hexString)
        }
        
        var deleteDate : Date? = nil
        if let date = json["destroy_date"] as? TimeInterval {
            deleteDate = Date(timeIntervalSince1970: date)
        }
        
        let note = Note(uuid: uuid, title: title, content: content, noteColor: noteColor, deleteDate: deleteDate)
        return note
    }
}

