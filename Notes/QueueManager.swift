//
//  QueueManager.swift
//  Notes
//
//  Created by Semen Meluzov on 24.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class QueueManager {
    
    private var mainQueue: OperationQueue = {
        var queue = OperationQueue.main
        return queue
    }()
    
    private var cacheQueue: OperationQueue = {
        var queue = OperationQueue()
        //queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    private var networkQueue: OperationQueue = {
        var queue = OperationQueue()
        //queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func add(operation: AsyncOperation) {

        switch operation {
        case is LoadCacheOperation:
            cacheQueue.addOperation(operation)
            print("Load")
        case is RequestNetworkOperation:
            networkQueue.addOperation(operation)
            print("Requast")
        case is SaveCacheOperation:
            cacheQueue.addOperation(operation)
            print("Save")
        case is PostToServerOperation:
            networkQueue.addOperation(operation)
            print("Post")
        case is UpdateCollectionViewOperation:
            mainQueue.addOperation(operation)
            print("Main")
        default:
            break
        }
    }
}
