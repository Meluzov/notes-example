//
//  NoteToNotePolicy.swift
//  Notes
//
//  Created by Semen Meluzov on 06.06.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CoreData

class NoteToNotePolicy: NSEntityMigrationPolicy {

    
    override func createDestinationInstances(forSource sInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        

        let newObject = NSEntityDescription.insertNewObject(forEntityName: mapping.destinationEntityName!, into: manager.destinationContext)
        
        let sourceKeys = sInstance.entity.attributesByName.keys
        sourceKeys.forEach { (key) in
            
            if key == "deleteDate" {
                
                let date = sInstance.value(forKey: key) as! NSDate?
                
                if date != nil {
                    let seconds = Double(date!.timeIntervalSince1970)
                    newObject.setValue(seconds, forKey: key)
                } else {
                    newObject.setValue(-1, forKey: key)
                }
                
            } else {
                let value = sInstance.value(forKey: key)
                if value != nil {
                    newObject.setValue(value, forKey: key)
                }
            }
            
            manager.associate(sourceInstance: sInstance, withDestinationInstance: newObject, for: mapping)
        }
    }
}
