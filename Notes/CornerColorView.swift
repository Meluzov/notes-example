//
//  CornerColorView.swift
//  Notes
//
//  Created by Semen Meluzov on 13.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class CornerColorView: UIView {
    
    var color : UIColor! {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        self.clipsToBounds = true
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        context.setFillColor(color.cgColor)
        context.move(to: CGPoint.zero)
        context.addLine(to: CGPoint(x: self.frame.maxX, y: 0))
        context.addLine(to: CGPoint(x: 0, y: self.frame.maxY))
        context.fillPath()
        
    }
}
