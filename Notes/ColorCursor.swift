//
//  ColorCursor.swift
//  Notes
//
//  Created by Semen Meluzov on 05.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class ColorCursorView: UIView {
    
    var selectedColor = UIColor.green
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
    
        
        context.setFillColor(selectedColor.cgColor)
        context.setStrokeColor(UIColor.black.cgColor)
        let strokeLineWidth : CGFloat = 1.0
        context.setLineWidth(strokeLineWidth)
        
        context.move(to: CGPoint(x: self.frame.width / 2, y: 0))
        context.addLine(to: CGPoint(x: self.frame.width / 2, y: self.frame.height))
        context.strokePath()
        
        context.move(to: CGPoint(x: 0, y: self.frame.height / 2))
        context.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height / 2))
        context.strokePath()
        
        let radius : CGFloat = self.frame.width / 3

        let center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        
        context.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
        context.drawPath(using: .fillStroke)
    }
    
    func update(position: CGPoint, color: UIColor) {
        update(position: position)
        update(color: color)
    }
    
    func update(position: CGPoint) {
        self.center = CGPoint(x: position.x, y: position.y)
    }
    func update(color: UIColor) {
        
        self.selectedColor = color
        self.setNeedsDisplay()
    }
}
