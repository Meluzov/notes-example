//
//  DeleteView.swift
//  Notes
//
//  Created by Semen Meluzov on 14.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class CrossView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        self.clipsToBounds = true
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let strokeWidth : CGFloat = 3
        context.setStrokeColor(UIColor.black.cgColor)
        context.setLineWidth(strokeWidth)
        let center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        let radius = self.frame.height / 2 - strokeWidth
        context.addArc(center: center, radius: radius, startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
        context.strokePath()
        
        let indent : CGFloat = radius / 2
        let midX = self.bounds.midX
        let midY = self.bounds.midY
        context.move(to: CGPoint(x: midX - indent, y: midY - indent))
        context.addLine(to: CGPoint(x: midX + indent, y: midY + indent))
        context.strokePath()
        
        context.move(to: CGPoint(x: midX + indent, y: midY - indent))
        context.addLine(to: CGPoint(x: midX - indent, y: midY + indent))
        context.strokePath()
        
    }

}
