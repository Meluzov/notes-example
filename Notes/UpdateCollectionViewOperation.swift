//
//  UpdateCollectionViewOperation.swift
//  Notes
//
//  Created by Semen Meluzov on 29.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class UpdateCollectionViewOperation: AsyncOperation {

    var collectionView : UICollectionView!
    init(collectionView: UICollectionView?) {
        self.collectionView = collectionView
    }
    
    override func main() {
        
        guard let collection = self.collectionView else {
            finishFailure()
            return
        }
        collection.reloadData()
        finishSuccess()
    }
}
