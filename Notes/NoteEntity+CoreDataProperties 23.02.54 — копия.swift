//
//  NoteEntity+CoreDataProperties.swift
//  
//
//  Created by Semen Meluzov on 04.06.17.
//
//

import Foundation
import CoreData


extension NoteEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NoteEntity> {
        return NSFetchRequest<NoteEntity>(entityName: "NoteEntity");
    }

    @NSManaged public var uuid: String?
    @NSManaged public var title: String?
    @NSManaged public var content: String?
    @NSManaged public var color: String?
    @NSManaged public var deleteDate: NSDate?

}
