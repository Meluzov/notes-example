//
//  ColorInfoView.swift
//  Notes
//
//  Created by Semen Meluzov on 06.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class ColorInfoView: UIView {
    
    @IBOutlet weak var colorView : UIView!
    @IBOutlet weak var colorCodeLabel : UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setBackground(color: UIColor) {
        colorView.backgroundColor = color
        colorCodeLabel.text = color.hexString
    }
    
}
