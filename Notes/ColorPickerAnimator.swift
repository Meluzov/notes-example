//
//  ColorPickerAnimator.swift
//  Notes
//
//  Created by Semen Meluzov on 11.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

class ColorPickerAnimator: NSObject {
    
    let isPresentation: Bool
    let duration = 1.0
    
    init(isPresentation: Bool) {
        self.isPresentation = isPresentation
        super.init()
    }
}

extension ColorPickerAnimator: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let key = isPresentation ? UITransitionContextViewControllerKey.to :
            UITransitionContextViewControllerKey.from
        guard let controller = transitionContext.viewController(forKey: key) else {return}
        
        if isPresentation {
            containerView.addSubview(controller.view)
        }
        
        controller.view.layer.cornerRadius = 10
        
        let presentedFrame = transitionContext.finalFrame(for: controller)
        var dismissedFrame = presentedFrame
        dismissedFrame.origin.y = dismissedFrame.origin.y + 20
        
        let initialFrame = isPresentation ? dismissedFrame : presentedFrame
        let finalFrame = isPresentation ? presentedFrame : dismissedFrame
        let initialAlpha : CGFloat = isPresentation ? 0.0 : 1.0
        let finalAlpha : CGFloat = isPresentation ? 1.0 : 0.0

        controller.view.alpha = initialAlpha
        controller.view.frame = initialFrame
        UIView.animate(withDuration: duration, animations: {
            controller.view.alpha = finalAlpha
            controller.view.frame = finalFrame
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
}
