//
//  NoteVC+CollectionDelegates.swift
//  Notes
//
//  Created by Semen Meluzov on 14.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit


// MARK: - UICollectionViewDataSource
extension NotebookViewController {
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        
        let count = dummyNoteBookCache != nil ? dummyNoteBookCache.notesCollection.count : 0
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                            for: indexPath) as? NoteCell else {return UICollectionViewCell()}
        
        let notesArray = Array(dummyNoteBookCache.notesCollection.values)
        let note = notesArray[indexPath.row]
        cell.cornerColorView.color = note.noteColor
        cell.titleLabel.text = note.title
        cell.contentLabel.text = note.content
        cell.deleteView.isHidden = !isEditMode
        
        return cell
    }
}

//MARK: - UICollectionViewDelegate
extension NotebookViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let notesArray = Array(dummyNoteBookCache.notesCollection.values)
        let note = notesArray[indexPath.row]

        if isEditMode {
            
            save(note: note, mode: .Delete)

        } else {
            pushToEditVC(note: note, mode: .Edit)
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension NotebookViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: widthPerItem, height: 150)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
