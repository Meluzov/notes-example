//
//  DummyNoteBook.swift
//  Notes
//
//  Created by Semen Meluzov on 22.04.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CocoaLumberjack

public class DummyNoteBook {
    
    private(set) var notesCollection : [String : Note]
    
    public static var filePath: String? {
        guard let dir =
            NSSearchPathForDirectoriesInDomains(.documentDirectory, .allDomainsMask,
                                                true).first
            else { return nil }
        
        let path = "\(dir)/notes.json"
        //DDLogInfo(path)
        return path
    }
    
    public init() {
        notesCollection = [String : Note]()
    }
    
    public func addNew(note: Note) {
        
        notesCollection[note.uuid] = note
        
        
        guard let date = note.deleteDate else { return }
        let interval = getRemoveInterval(date: date)
        
        let queue = DispatchQueue.global(qos: .userInteractive)
        queue.asyncAfter(deadline: DispatchTime.now() + interval, execute: {
            
            DispatchQueue.main.async {[weak self] in
                
                guard let strongSelf = self else {return}
                strongSelf.deleteNote(uuid: note.uuid)
                
                print("Delete Note")
                //Нужно как то останавливать queue, или не захватывать note
                //Если вручную удалить note, то она останется в памяти пока queue не отработает
            }
        })
        
    }
    
    private func getRemoveInterval(date: Date) -> TimeInterval {
        
        let timeInterval = date.timeIntervalSince(Date())
        return timeInterval
    }
    
    public func deleteNote(uuid : String) {
        
        DDLogInfo("Note was deleted")
        notesCollection.removeValue(forKey: uuid)
    }
    
    public func deleteAll() {
        notesCollection.removeAll()
    }
    
    public func saveAll() {
        
        var allNotes = [[String : Any]]()
        
        for (_,note) in notesCollection {
            allNotes.append(note.json)
        }
        
        guard let path = DummyNoteBook.filePath else {
            DDLogInfo("file doesn't exist")
            return
        }
        let url = URL(fileURLWithPath: path)
        var data : Data = Data()
        let format = PropertyListSerialization.PropertyListFormat.xml
        do {
            data = try PropertyListSerialization.data(fromPropertyList: allNotes, format: format, options: 0)
            try data.write(to: url, options: .atomic)
        } catch  {
            DDLogInfo("Can not get data from JSON")
        }
        
        print("File Saved")
    }
    
    public func loadFile() {
        
        guard let path = DummyNoteBook.filePath else {
            DDLogInfo("file doesn't exist")
            return
        }
        let url = URL(fileURLWithPath: path)
        var data = Data()
        var propertyList = [[String : Any]]()
        do {
            data = try Data(contentsOf: url)
            propertyList = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [[String : Any]]
        } catch  {
            DDLogInfo("Bad data by path")
        }
        
        for noteData in propertyList {
            
            guard let note = Note.parse(json: noteData) else {
                DDLogInfo("Bad Parse")
                return
            }
            addNew(note: note)
        }
    }
    
    public func printNotes() {
        for (_,note) in notesCollection {
            DDLogInfo("Uuid = \(note.uuid)")
            DDLogInfo("Title = \(note.title)")
            DDLogInfo("Content = \(note.content)")
            DDLogInfo("Color = \(note.noteColor)")
            DDLogInfo("Delete date = \(note.deleteDate)\n")
        }
        if notesCollection.count == 0 {
            DDLogInfo("No Notes\n")
            
        }
    }
}

