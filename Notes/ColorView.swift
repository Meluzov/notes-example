//
//  PaletteColorView.swift
//  Notes
//
//  Created by Semen Meluzov on 05.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

@IBDesignable class ColorView: UIView {
    
    weak var delegate: ColorViewDelegate?
    
    @IBOutlet weak var indicatorView: SelectionIndicatorView!
    var paletteView : PaletteView?
    
    var nibView : UIView!
    
    var color : UIColor {
        return nibView.backgroundColor!
    }
    
    var isSelected : Bool = false {
        didSet {
            guard let indicator = indicatorView else {
                return
            }
            indicator.isHidden = !isSelected
        }
    }
    
    @IBInspectable var backColor: UIColor? {
        didSet {
            nibView.backgroundColor = backColor
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    
    func commonInit() {
        
        guard let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last else {
            return
        }
        guard let nibView = Bundle(for: type(of: self)).loadNibNamed(nibName, owner: self, options: nil)?.last as? UIView else {
            return
        }
        self.nibView = nibView
        addSubview(self.nibView)
        self.nibView.frame = bounds
        self.nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ColorView.tapGesture(sender:)))
        addGestureRecognizer(tapGestureRecognizer)
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(ColorView.longPressGesture(sender:)))
        addGestureRecognizer(longPressGestureRecognizer)
    }

    func addPaletteView() {
        paletteView = PaletteView(frame: self.bounds)
        if paletteView != nil {
            nibView.insertSubview(paletteView!, at: 0)
        }
    }
    
    func removePaletteView() {
        if paletteView != nil {
            paletteView?.removeFromSuperview()
            paletteView = nil
        }
    }
    
    //MARK: - UIGestureRecognizer
    func tapGesture(sender: UITapGestureRecognizer) {
        
        delegate?.didTapTo(sender: self)
    }
    
    func longPressGesture(sender: UITapGestureRecognizer) {
        
        if sender.state == .began {
            self.delegate?.didLongPress(sender: self)
        }
    }
}

protocol ColorViewDelegate: class {
    
    func didTapTo(sender: ColorView)
    func didLongPress(sender: ColorView)
}
