//
//  CoreDataManager.swift
//  Notes
//
//  Created by Semen Meluzov on 04.06.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
    private override init() { }
    
    static let shared = CoreDataManager()

    
    func migration() {
        
        var sourceMetadata : [String : Any]? = nil
        let  sourceStoreType = NSSQLiteStoreType
        do {
            
            sourceMetadata = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: self.sourceUrl!, options: nil)

        } catch {
            return
        }
        
        var isMigrationNeeded = false
        
        guard let sMetadata = sourceMetadata else {
            return
        }
        
        let destinationModel = self.managedObjectModel
        
        isMigrationNeeded = !destinationModel.isConfiguration(withName: nil, compatibleWithStoreMetadata: sMetadata)
        
        if isMigrationNeeded {
            
            let sourceModel = NSManagedObjectModel.mergedModel(from: [Bundle.main], forStoreMetadata: sMetadata)
            guard let sModal = sourceModel else {
                return
            }
            
            let migrationManager = NSMigrationManager.init(sourceModel: sModal, destinationModel: destinationModel)
            
            let mappingUrl = Bundle.main.url(forResource: "MappingModel", withExtension: "cdm")
            
            let mappingModel = NSMappingModel(contentsOf: mappingUrl)

            guard let mapModal = mappingModel else {
                return
            }
            
            let destinationStoreURL = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreDataNew.sqlite")
            
            let destinationStoreType = NSSQLiteStoreType
            
            do {
                
                try migrationManager.migrateStore(from: sourceUrl!, sourceType: sourceStoreType, options: nil, with: mapModal, toDestinationURL: destinationStoreURL!, destinationType: destinationStoreType, destinationOptions: nil)
                
                
                print("Migrating from source: \(sourceUrl!.path) ===To=== \(destinationStoreURL?.path)")
                
                // Delete old sqlite file
                let fileManager = FileManager.default
                let shmString = "\(sourceUrl!.path)-shm"
                let walString = "\(sourceUrl!.path)-wal"
                do {
                    
                    try fileManager.removeItem(at: sourceUrl!)
                    try fileManager.removeItem(at: URL(string: shmString)!)
                    try fileManager.removeItem(at: URL(string: walString)!)

                } catch let error {
                    print(error.localizedDescription)
                }
                
                // Copy into new location
                
                do {
                    try fileManager.moveItem(at: destinationStoreURL!, to: sourceUrl!)

                } catch let error {
                    print(error.localizedDescription)
                }
                
                
                print("MIGRATION SUCCESS")
            } catch let error {
                print(error.localizedDescription)
                return
            }
            
        } else {
            return
        }
    }
    
    lazy var applicationDocumentsDirectory: NSURL = {
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var  sourceUrl : URL? = {
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        return url
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "NotesCoreData", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {

        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)

        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: self.sourceUrl, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)

            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {

        let coordinator = self.persistentStoreCoordinator

        var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {

                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}
