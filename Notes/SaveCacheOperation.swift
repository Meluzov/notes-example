//
//  SaveCacheOperation.swift
//  Notes
//
//  Created by Semen Meluzov on 24.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit
import CoreData

enum Mode {
    case Save
    case Delete
    case Edit
}

class SaveCacheOperation: AsyncOperation {
    
    var notes: [Note]!
    var mode: Mode
    
    var managedObjectContext : NSManagedObjectContext {
        return CoreDataManager.shared.managedObjectContext
    }
    
    init(withNotes notes: [Note], mode: Mode) {
        self.notes = notes
        self.mode = mode
    }
    
    override func main() {
        
        managedObjectContext.performAndWait {[weak self] in
            
            guard let sself = self else { return }
            
            let descriprion = NSEntityDescription.entity(forEntityName: "NoteEntity", in: sself.managedObjectContext)
            
            switch sself.mode {
            case .Save:
                
                for note in sself.notes {
                    
                    let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
                    request.predicate = NSPredicate(format: "uuid == %@", note.uuid)
                    do {
                        let notesEntity = try sself.managedObjectContext.fetch(request)
                        if notesEntity.isEmpty {
                            
                            var noteEntity = NoteEntity(entity: descriprion!, insertInto: sself.managedObjectContext)
                            
                            noteEntity = NoteStore.convert(note: note, toEntity: noteEntity)
                            
                            if dummyNoteBookCache == nil {
                                dummyNoteBookCache = DummyNoteBook()
                            }
                            dummyNoteBookCache.addNew(note: note)
                        }
                        
                    } catch {
                        sself.finishFailure()
                        return
                    }
                }
                
            case .Delete:
                
                for note in sself.notes {
                    
                    let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
                    request.predicate = NSPredicate(format: "uuid == %@", note.uuid)
                    do {
                        let notesEntity = try sself.managedObjectContext.fetch(request)
                        guard let noteEntity = notesEntity.first else {
                            sself.finishFailure()
                            return
                        }
                        sself.managedObjectContext.delete(noteEntity)
                        guard let uuid = noteEntity.uuid else {
                            sself.finishFailure()
                            return
                        }
                        dummyNoteBookCache.deleteNote(uuid: uuid)
                    } catch {
                        sself.finishFailure()
                        return
                    }
                }
                
            case .Edit:
                
                for note in sself.notes {
                    
                    let request: NSFetchRequest<NoteEntity> = NoteEntity.fetchRequest()
                    request.predicate = NSPredicate(format: "uuid == %@", note.uuid)
                    do {
                        let notesEntity = try CoreDataManager.shared.managedObjectContext.fetch(request)
                        guard var noteEntity = notesEntity.first else {
                            sself.finishFailure()
                            return
                        }
                        
                        noteEntity = NoteStore.convert(note: note, toEntity: noteEntity)
                        dummyNoteBookCache.addNew(note: note)
                    } catch {
                        sself.finishFailure()
                        return
                    }
                }
                
            }
            
            do {
                try sself.managedObjectContext.save()
                sself.finishSuccess()
            } catch {
                sself.finishFailure()
                return
            }

        }
        
    }
}
