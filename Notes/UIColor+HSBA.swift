//
//  UIColor+HSBA.swift
//  Notes
//
//  Created by Semen Meluzov on 06.05.17.
//  Copyright © 2017 Semen Meluzov. All rights reserved.
//

import UIKit

extension UIColor {
    
    var hsba: (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) {
        var hsba: (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) = (0, 0, 0, 0)
        self.getHue(&(hsba.h), saturation: &(hsba.s), brightness: &(hsba.b), alpha: &(hsba.a))
        return hsba
    }
    
}
